package uj.java.pwj2019.w3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ListMerger {
    public static List<Object> mergeLists(List<?> l1, List<?> l2) {
        ArrayList<Object> zippedList = new ArrayList<Object>();

        if(l1 == null && l2 == null) {
            List<Object> zippedListImmutabe = Collections.unmodifiableList(zippedList);
            return zippedListImmutabe;
        }else {
            for(int i = 0; i < Math.min(l1.size(), l2.size()); i++) {
                zippedList.add(l1.get(i));
                zippedList.add(l2.get(i));
            }

            for(int i = Math.min(l1.size(), l2.size()); i < l1.size(); i++) {
                zippedList.add(l1.get(i));
            }

            for(int i = Math.min(l1.size(), l2.size()); i < l2.size(); i++) {
                zippedList.add(l2.get(i));
            }
        }
        List<Object> zippedListImmutabe = Collections.unmodifiableList(zippedList);
        return zippedListImmutabe;
//        return IntStream.range(0, Math.min(l1.size(), l2.size()))
//                .boxed()
//                .collect(Collectors.toMap(l1::get, l2::get))
//                .entrySet()
//                .stream()
//                .map(x -> List.of(x.getKey(), x.getValue()))
//                .flatMap(List::stream)
//                .collect(Collectors.toList());
    }

}
