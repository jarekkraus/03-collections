package uj.java.pwj2019.w3;

import javax.swing.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;


public interface JsonMapper {

    String toJson(Map<String, ?> map);

    static JsonMapper defaultInstance() {
        JsonString stringJson = new JsonString();
        return stringJson;
    }
}

class JsonString implements JsonMapper {

    private String eval(Object obj) {
        if(obj instanceof String) return "\"" + ((String) obj).replace("\"","\\\"") + "\"";
        if(obj instanceof Boolean) return "" + obj;
        if(obj instanceof Number) return String.valueOf(obj);
        if(obj.equals(Collections.emptyList())) return "[]";

        if(obj instanceof Map) {
            Map<String, ?> subMap = (Map<String, ?>)obj;
            StringBuilder result = new StringBuilder("{");
            for(Map.Entry<String, ?> entry : subMap.entrySet()) {
                result.append("\"" + entry.getKey() + "\":");
                result.append(eval(entry.getValue()));
                result.append(",");
            }
            result.setLength(result.length() - 1);
            result.append("}");
            return result.toString();
        }
        if(obj instanceof List) {
            List<?> subList = (List<?>)obj;
            StringBuilder result = new StringBuilder("[");
            for(Object el : subList) {
                result.append(eval(el));
                result.append(",");
            }
            result.setLength(result.length() - 1);
            result.append("]");
            return result.toString();
        }
        return null;
    };

    @Override
    public String toJson(Map<String, ?> map) {
        if(map == null || map.isEmpty()) {
            return "{}";
        }else {
             return eval(map);
        }
    }
}